package models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import entities.BookCopy;

public class BookCopyAccess {
	
	private static Connection myCon;
	
	public BookCopyAccess() {
		try {
			myCon = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "abc123");
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
	}
	
	public BookCopy add(BookCopy copy, int book_id) {
		try {
			PreparedStatement ps = myCon.prepareStatement("INSERT INTO book_copies (book_id,copy_number) VALUES(?,?)",Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, book_id);
			ps.setString(2, copy.getCopyNumber());
			ps.executeUpdate();
			
			ResultSet rs = ps.getGeneratedKeys();
			int copy_id = 0;
			if(rs.next())
				copy_id = rs.getInt(1);
			copy.setId(copy_id);
			
			return copy;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
		
		return null;
	}
	
	public BookCopy get(int id) {
		try {
			Statement statement = myCon.createStatement();
			
			BookCopy copy = new BookCopy();
			ResultSet RS2 = statement.executeQuery("SELECT * FROM book_copies WHERE id="+id);
			while(RS2.next()) {
				copy.setId(id);
				copy.setCopyNumber(RS2.getString("copy_number"));
			}
	
			return copy;		
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}				
		return null;
	}
	
	public BookCopy update(BookCopy copy, int book_id) {

		try {
			PreparedStatement ps = myCon.prepareStatement("UPDATE book_copies SET book_id=?,copy_number=? WHERE id=?");
			ps.setInt(1, book_id);
			ps.setString(2, copy.getCopyNumber());
			ps.setInt(5, copy.getId());
			ps.executeUpdate();			
			
			return copy;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
		
		return null;
	}
	
	public BookCopy delete(BookCopy copy) {
		
		try {
			PreparedStatement ps = myCon.prepareStatement("DELETE FROM book_copies WHERE id=?");
			ps.setInt(1, copy.getId());
			ps.executeUpdate();			
			
			return copy;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
		
		return null;
	}

}

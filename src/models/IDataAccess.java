package models;

public interface IDataAccess<K, V> {
	public V create(K key, V value);
	public V read(K key);
	public V update(K key, V value);
	public V delete(K key);
}

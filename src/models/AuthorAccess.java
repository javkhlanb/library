package models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entities.*;

public class AuthorAccess {

private static Connection myCon;
	
	public AuthorAccess() {
		try {
			myCon = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "abc123");
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
	}
	
	public Author add(Author author) {
		try {
			Address address = new AddressAccess().add(author.getAddress());
			
			PreparedStatement ps = myCon.prepareStatement("INSERT INTO authors (first_name,last_name,phone,address_id,bio) VALUES(?,?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, author.getFirstName());
			ps.setString(2, author.getLastName());
			ps.setString(3, author.getPhone());
			ps.setInt(4, address.getId());
			ps.setString(5, author.getBio());
			ps.executeUpdate();
			
			ResultSet rs = ps.getGeneratedKeys();
			int id = 0;
			if(rs.next())
				id = rs.getInt(1);
			author.setId(id);
			return author;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Author get(int id) {
		try {
			Statement statement = myCon.createStatement();
			ResultSet RS = statement.executeQuery("SELECT * FROM authors WHERE id="+id);
			while(RS.next()) {
				Author author = new Author();
				author.setId(RS.getInt("id"));
				author.setFirstName(RS.getString("first_name"));
				author.setLastName(RS.getString("last_name"));
				author.setPhone(RS.getString("phone"));
				author.setBio(RS.getString("bio"));
				
				int addr = RS.getInt("address_id");
				if(addr > 0) {
					Address address = new AddressAccess().get(addr);
					author.setAddress(address);
				}
				return author;
			}
			
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
				
		return null;
	}
	
	public List<Author> getAll(Book book) {
		List<Author> authors = new ArrayList<Author>();
		try {
			Statement statement = myCon.createStatement();
			ResultSet RS = statement.executeQuery("SELECT * FROM book_authors WHERE book_id="+book.getId());
			while(RS.next()) {
				int author_id = RS.getInt("author_id");
				ResultSet RS2 = statement.executeQuery("SELECT * FROM authors WHERE id="+author_id);
				while(RS.next()) {
				
				
					Author author = new Author();
					author.setId(RS.getInt(RS2.getInt("id")));
					author.setFirstName(RS2.getString("first_name"));
					author.setLastName(RS2.getString("last_name"));
					author.setPhone(RS2.getString("phone"));
					author.setBio(RS2.getString("bio"));
					
					int addr = RS2.getInt("address_id");
					if(addr > 0) {
						Address address = new AddressAccess().get(addr);
						author.setAddress(address);
					}
					 authors.add(author);
				}
			}
			return authors;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}		
		
		return null;
	}
	
	public Author update(Author author) {

		try {
			Address address = new AddressAccess().update(author.getAddress());

			PreparedStatement ps = myCon.prepareStatement("UPDATE authors SET first_name=?,last_name=?,phone=?,address_id=?,bio=? WHERE id=?");
			ps.setString(1, author.getFirstName());
			ps.setString(2, author.getLastName());
			ps.setString(3, author.getPhone());
			ps.setInt(4, author.getAddress().getId());
			ps.setInt(6, author.getId());
			ps.setString(5, author.getBio());
			ps.executeUpdate();
			
			author.setAddress(address);
			return author;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Author delete(Author authors) {
		
		try {
			PreparedStatement ps = myCon.prepareStatement("DELETE FROM authors  WHERE id=?");
			ps.setInt(1, authors.getId());
			ps.executeUpdate();
			
			Address address = new AddressAccess().delete(authors.getAddress());
			ps = myCon.prepareStatement("DELETE FROM addresses WHERE id=?");
			ps.setInt(1, authors.getAddress().getId());
			ps.executeUpdate();		
			
			authors.setAddress(address);
			return authors	;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
		
		return null;
	}

}

package models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import entities.*;

public class BookAccess {
	
private static Connection myCon;
	
	public BookAccess() {
		try {
			myCon = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "abc123");
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
	}
	
	public Book add(Book book) {
		try {
			String avlb = (book.isAvailability())? "AVAILABLE": "UNAVAILABLE";
			PreparedStatement ps = myCon.prepareStatement("INSERT INTO books (title,no_of_days,isbn,availability) VALUES(?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, book.getTitle());
			ps.setInt(2, book.getNumberOfDays());
			ps.setString(3, book.getIsbn());
			ps.setString(4, avlb);
			ps.executeUpdate();
			
			ResultSet rs = ps.getGeneratedKeys();
			int book_id = 0;
			if(rs.next())
				book_id = rs.getInt(1);
			
			int count = 0;
			for(BookCopy copy : book.getCopies()) {
				BookCopy cpy = new BookCopyAccess().add(copy, book_id);
				book.getCopies().set(count, cpy);
				count++;
			}
			
			count = 0;
			for(Author author : book.getAuthors()) {
				Author aut = new AuthorAccess().add(author);
				addBookAuthor(book_id, aut.getId());			
				book.getAuthors().set(count, aut);
				count++;
			}
			
			
			
			return book;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
		
		return null;
	}
	
	private void addBookAuthor(int bid, int aid) {
		PreparedStatement ps;
		try {
			ps = myCon.prepareStatement("INSERT INTO book_authors (book_id,author_id) VALUES(?,?)",Statement.RETURN_GENERATED_KEYS);
			ps.setInt(1, bid);
			ps.setInt(2, aid);
			ps.executeUpdate();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public Book get(int id) {
		try {
			Statement statement = myCon.createStatement();
			ResultSet RS = statement.executeQuery("SELECT * FROM books WHERE id="+id);
			while(RS.next()) {
				Book book = new Book();
				book.setId(RS.getInt("id"));
				book.setIsbn(RS.getString("isbn"));
				book.setTitle(RS.getString("title"));
				book.setNumberOfDays(RS.getInt("no_of_days"));
				boolean avlb = (RS.getString("availability").equals("AVAILABLE"))? true : false;
				book.setAvailability(avlb);
				
				int book_id = RS.getInt("id");
				
					
				ResultSet RS2 = statement.executeQuery("SELECT * FROM book_copies WHERE book_id="+book_id);
				while(RS2.next()) {
					BookCopy copy = new BookCopyAccess().get(RS2.getInt("id"));
					book.getCopies().add(copy);
				}
				
				RS2 = statement.executeQuery("SELECT * FROM book_authors WHERE book_id="+book_id);
				while(RS2.next()) {
					Author author = new AuthorAccess().get(RS2.getInt("author_id"));
					book.getAuthors().add(author);
				}
				
				return book;
			}
			
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
				
		return null;
	}
	
	public List<Book> getAll() {
		List<Book> books = new ArrayList<Book>();
		try {
			Statement statement = myCon.createStatement();
			ResultSet RS = statement.executeQuery("SELECT * FROM books");
			while(RS.next()) {
				Book book = get(RS.getInt("id"));
				
				 books.add(book);
			}
			return books;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}		
		
		return null;
	}
	
	public Book update(Book book) {

		try {
			String avlb = (book.isAvailability())? "AVAILABLE": "UNAVAILABLE";
			PreparedStatement ps = myCon.prepareStatement("UPDATE books SET title=?,no_of_days=?,isbn=?,availability=? WHERE id=?");
			ps.setString(1, book.getTitle());
			ps.setInt(2, book.getNumberOfDays());
			ps.setString(3, book.getIsbn());
			ps.setString(4, avlb);
			ps.setInt(5, book.getId());
			ps.executeUpdate();			

			int count = 0;
			for(Author author : book.getAuthors()) {
				Author aut = new AuthorAccess().update(author);
				book.getAuthors().set(count, aut);
				count++;
			}
			
			count = 0;
			for(BookCopy copy : book.getCopies()) {
				BookCopy cpy = new BookCopyAccess().update(copy, book.getId());
				book.getCopies().set(count, cpy);
				count++;
			}
			
			return book;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Book delete(Book book) {
		
		try {
			PreparedStatement ps = myCon.prepareStatement("DELETE FROM books  WHERE id=?");
			ps.setInt(1, book.getId());
			ps.executeUpdate();
			
			ps = myCon.prepareStatement("DELETE FROM book_copies WHERE book_id=?");
			ps.setInt(1, book.getId());
			ps.executeUpdate();	
			
			ps = myCon.prepareStatement("DELETE FROM book_authors WHERE book_id=?");
			ps.setInt(1, book.getId());
			ps.executeUpdate();	
			
			for(Author author : book.getAuthors()) {
				Author aut = new AuthorAccess().delete(author);
			}
			
			return book;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
		
		return null;
	}

	
}

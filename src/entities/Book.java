package entities;

import java.util.*;

public class Book {
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getNumberOfDays() {
		return numberOfDays;
	}
	public void setNumberOfDays(int numberOfDays) {
		this.numberOfDays = numberOfDays;
	}
	public String getIsbn() {
		return isbn;
	}
	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}
	public List<Author> getAuthors() {
		return authors;
	}
	public void setAuthors(List<Author> authors) {
		this.authors = authors;
	}
	public boolean isAvailability() {
		return availability;
	}
	public void setAvailability(boolean availability) {
		this.availability = availability;
	}
	public List<BookCopy> getCopies() {
		return copies;
	}
	public void setCopies(List<BookCopy> copies) {
		this.copies = copies;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	int id;
	String title;
	int numberOfDays;
	String isbn;
	List <Author> authors;
	boolean availability;
	List<BookCopy> copies;
	
	int countbook = 0;
	public Book(String title, int numberOfDays, String isbn, List<Author> authors, boolean availability,
			List<BookCopy> copies) {
		super();
		this.title = title;
		this.numberOfDays = numberOfDays;
		this.isbn = isbn;
		this.authors = authors;
		this.availability = availability;
		this.copies = copies;
	}
	
	public Book() {
		
	}
	
	void addBookCopy()
	{
		countbook++;
		BookCopy temp = new BookCopy();
		temp.setId(countbook);
		temp.setCopyNumber(String.valueOf(countbook * 2));
		temp.setBookId(id);
	}
	
}

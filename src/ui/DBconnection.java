package ui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import entities.Role;
import entities.User;

public class DBconnection {
	
	private static final String DRIVER="com.mysql.jdbc.Driver";
	private static final String URL="jdbc:mysql://localhost:3306/library";
	
	
	private static Connection connection=null;
	
	 public static Connection createConnection() throws ClassNotFoundException, SQLException {
	           Class.forName(DRIVER);
	        
	           if(connection !=null)
	        	   return connection;
	           
	            return DriverManager.getConnection(URL, "root", "");
	    }

	 private DBconnection() {
		 
	 }
	 
	 public static User getUser(int ID) throws ClassNotFoundException, SQLException {
		 
		 String sql="select * from users where user_id = ? limit ?";
		 
		 List<Object> parameters=new ArrayList<>();
		 parameters.add(ID);
		 parameters.add(1);
		 List<Map<String, Object>> res=query(createConnection(), sql, parameters);
		  if(res !=null) {
			  
			  User user=new User();
			  
			  for(Map<String, Object> temp  : res) {
				 
				 user.setId((Integer) temp.get("id"));
				user.setFirst_name((String) temp.get("first_name"));
				user.setUser_id((Integer) temp.get("user_id"));
				 user.setLast_name((String) temp.get("last_name"));
				  user.setRole(Role.valueOf((String) temp.get("role_id")));
				  user.setPassword((String) temp.get("password"));
				  
			  }
			  
			  if(user.getFirst_name() ==null)
				  return null;
				  
			  
			 return user;
			 
			 
			  
		  }
		 
		 return null;
		 
	 }
	 
	    public static void close(Connection connection) {
	        try {
	            if (connection != null) {
	                connection.close();
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }
	    
	    
	    


	    public static void close(Statement st) {
	        try {
	            if (st != null) {
	                st.close();
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }

	    public static void close(ResultSet rs) {
	        try {
	            if (rs != null) {
	                rs.close();
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	    }

	 
	    public static List<Map<String, Object>> map(ResultSet rs) throws SQLException {
	        List<Map<String, Object>> results = new ArrayList<Map<String, Object>>();
	        try {
	            if (rs != null) {
	                ResultSetMetaData meta = rs.getMetaData();
	                int numColumns = meta.getColumnCount();
	                while (rs.next()) {
	                    Map<String, Object> row = new HashMap<String, Object>();
	                    for (int i = 1; i <= numColumns; ++i) {
	                        String name = meta.getColumnName(i);
	                        Object value = rs.getObject(i);
	                        row.put(name, value);
	                    }
	                    results.add(row);
	                }
	            }
	        } finally {
	            close(rs);
	        }
	        return results;
	    }

	    public static List<Map<String, Object>> query(Connection connection, String sql, List<Object> parameters) throws SQLException {
	        List<Map<String, Object>> results = null;
	        PreparedStatement ps = null;
	        ResultSet rs = null;
	        try {
	            ps = connection.prepareStatement(sql);

	            int i = 0;
	            for (Object parameter : parameters) {
	                ps.setObject(++i, parameter);
	            }
	            rs = ps.executeQuery();
	            results = map(rs);
	        } finally {
	            close(rs);
	            close(ps);
	        }
	        return results;
	    }

	    public static int update(Connection connection, String sql, List<Object> parameters) throws SQLException {
	        int numRowsUpdated = 0;
	        PreparedStatement ps = null;
	        try {
	            ps = connection.prepareStatement(sql);

	            int i = 0;
	            for (Object parameter : parameters) {
	                ps.setObject(++i, parameter);
	            }
	            numRowsUpdated = ps.executeUpdate();
	        } finally {
	            close(ps);
	        }
	        return numRowsUpdated;
	    }
}

package controllers;

import java.util.*;
import models.*;
import entities.*;
public class BookController {
	
	private static BookController instance = new BookController();
	
	private BookController(){
	}
	
	public static BookController getInstance() {
		return instance;
	}
		
	private static BookAccess data = new BookAccess();
	
	public List<Book> index(){
		List<Book> b = data.getAll();
		return b;
	}
	
	public Book fetch(int id) {
		Book member = data.get(id);
		return member;
	}
	
	public Book create(String title, int numberOfDays, String isbn, List<Author> authors, boolean availability,
			List<BookCopy> copies)
	{
		Book member = new Book( title, numberOfDays, isbn, authors, availability, copies);
		return member;
	}
	public Book update(Book book, String title, int numberOfDays, String isbn, List<Author> authors, boolean availability,
			List<BookCopy> copies)
	{
		book.setTitle(title);
		book.setNumberOfDays(numberOfDays);
		book.setIsbn(isbn);
		book.setAuthors(authors);
		book.setAvailability(availability);
		book.setCopies(copies);
		return book;
	}
	
	public Book delete(Book book) {
		book = data.delete(book);
		return book;
	}
}

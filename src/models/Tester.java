package models;

import entities.Address;
import entities.LibraryMember;

public class Tester {

	public static void main(String[] args) {
		MemberAccess MA = new MemberAccess();
		
//		Address address = new Address();
//		address.setStreet("10th Street N");
//		address.setCity("Chicago");
//		address.setState("Illinois");
//		address.setZip("55433");
//		
//		LibraryMember member = new LibraryMember();
//		member.setFirstName("Edwin");
//		member.setLastName("Oigo");
//		member.setPhone("00198383483");
//		member.setAddress(address);
		
		LibraryMember member = MA.get(3);
		member.setLastName("Bono Oigo");
		
		LibraryMember rmember = MA.update(member);
		System.out.println(rmember.getId()+": "+rmember.getFirstName()+" "+rmember.getLastName()+" lives in "+rmember.getAddress().getCity());
		
		String city = "Chicago";
		String street = "10th Street N";
		String state = "Illinois";
		String zip = "55433";
		System.out.println("INSERT INTO addresses (street,city,state,zip) VALUES("+street+","+city+","+state+","+zip+")");
		
//		LibraryMember M = MA.get(1);
//		System.out.println(M.getFirstName()+" "+M.getLastName()+" lives in "+M.getAddress().getCity());

	}

}

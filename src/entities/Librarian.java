package entities;

import models.DataAccess;

public class Librarian {
	private DataAccess<Integer, LibraryMember> libraryMemberData;
	private DataAccess<String, BookCopy> bookCopyData;
	public Librarian() {
		libraryMemberData = new DataAccess<Integer, LibraryMember>();
	}
	public void checkOutBook(int libraryMemberId, String bookCopyId) throws Exception {
		LibraryMember member = libraryMemberData.read(libraryMemberId);
		BookCopy copy = bookCopyData.read(bookCopyId);
		if (member == null) throw new Exception("Library Member Not Found");
		if (copy == null) throw new Exception("Book Copy Not Found");
		member.checkOut(copy);
	}
	public void checkInBook(int libraryMemberId, String bookCopyNumber) throws Exception {
		LibraryMember member = libraryMemberData.read(libraryMemberId);
		if (member == null) throw new Exception("Library Member Not Found");
		member.checkIn(bookCopyNumber);
	}
	
}

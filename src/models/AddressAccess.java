package models;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import entities.Address;

public class AddressAccess {

private static Connection myCon;
	
	public AddressAccess() {
		try {
			myCon = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "abc123");
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
	}
	
	public Address add(Address address) {
		try {
			PreparedStatement ps = myCon.prepareStatement("INSERT INTO addresses (street,city,state,zip) VALUES(?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, address.getStreet());
			ps.setString(2, address.getCity());
			ps.setString(3, address.getState());
			ps.setString(4, address.getZip());
			ps.executeUpdate();
			
			ResultSet rs = ps.getGeneratedKeys();
			int addr_id = 0;
			if(rs.next())
				addr_id = rs.getInt(1);
			address.setId(addr_id);
			
			return address;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Address get(int id) {
		try {
			Statement statement = myCon.createStatement();
			
			Address address = new Address();
			ResultSet RS2 = statement.executeQuery("SELECT * FROM addresses WHERE id="+id);
			while(RS2.next()) {
				address.setId(id);
				address.setStreet(RS2.getString("street"));
				address.setCity(RS2.getString("city"));
				address.setState(RS2.getString("state"));
				address.setZip(RS2.getString("zip"));
			}
	
			return address;		
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}				
		return null;
	}
	
	public Address update(Address address) {

		try {
			PreparedStatement ps = myCon.prepareStatement("UPDATE addresses SET street=?,city=?,state=?,zip=? WHERE id=?");
			ps.setString(1, address.getStreet());
			ps.setString(2, address.getCity());
			ps.setString(3, address.getState());
			ps.setString(4, address.getZip());
			ps.setInt(5, address.getId());
			ps.executeUpdate();			
			
			return address;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
		
		return null;
	}
	
	public Address delete(Address address) {
		
		try {
			PreparedStatement ps = myCon.prepareStatement("DELETE FROM addresses WHERE id=?");
			ps.setInt(1, address.getId());
			ps.executeUpdate();			
			
			return address;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
		
		return null;
	}
	
}

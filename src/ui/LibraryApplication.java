package ui;



import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class LibraryApplication  extends Application {
	
	
	 @Override
	    public void start(Stage stage) throws Exception {
	       Parent root = FXMLLoader.load(getClass().getResource("login.fxml"));
	    
	        Scene scene = new Scene(root, 600, 450);
	        stage.setTitle("Library System");
	        stage.setScene(scene);
	        stage.show();
	    }

	 
	 public static void main(String[] args) {
		 launch(args);
	 }
	

}

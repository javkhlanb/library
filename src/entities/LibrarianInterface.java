package entities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javafx.application.Application;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;

import models.MemberAccess;

public class LibrarianInterface extends Application {
	private final MemberAccess conn;
	private TableView tv;
	private List<LibraryMember> members;
	private ObservableList<TableEntry> data = 
			FXCollections.observableArrayList(
            new TableEntry(1, "book1", new Date(), new Date(), null),
            new TableEntry(2, "book1", new Date(), new Date(), new Date()),
            new TableEntry(3, "book2", new Date(), new Date(), new Date())
        );
   
	private static Librarian librarian;
	public LibrarianInterface () {
		librarian = new Librarian();
		conn = new MemberAccess();
		this.collectData();
	}
	
	public static void main(String[] args) {
		launch(args);
	}
	
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		GridPane gp = createGridPane();
		createTableView();
		VBox vb = new VBox();
		vb.setPadding(new Insets(10, 10, 10, 10));
		vb.getChildren().addAll(gp, tv);
		
		primaryStage.setTitle("Librarian System");
		primaryStage.setScene(new Scene(vb, 615, 600));
		primaryStage.show();
	}
	
	private void createTableView() {
		this.tv = new TableView();
		this.tv.setEditable(true);
		TableColumn member = new TableColumn("Member Id");
		TableColumn copy = new TableColumn("BookCopy Number");
		TableColumn checkout = new TableColumn("Checkout Date");
		TableColumn duedate = new TableColumn("Due Date");
		TableColumn checkin = new TableColumn("Checkin Date");
		
		member.setCellValueFactory(new PropertyValueFactory<TableEntry, String>("libraryMemberId"));
		copy.setCellValueFactory(new PropertyValueFactory<TableEntry, String>("bookCopyNumber"));
		checkout.setCellValueFactory(new PropertyValueFactory<TableEntry, String>("checkOutDate"));
		duedate.setCellValueFactory(new PropertyValueFactory<TableEntry, String>("dueDate"));
		checkin.setCellValueFactory(new PropertyValueFactory<TableEntry, String>("checkInDate"));
		
		tv.setItems(data);
		tv.getColumns().addAll(member, copy, checkout, duedate, checkin);
		
	}
	
	private GridPane createGridPane() {
		GridPane gp = new GridPane();
		gp.setAlignment(Pos.CENTER);
		gp.setPadding(new Insets(20, 20, 20, 20));
		gp.setHgap(10);
		//gp.setVgap(10);
		
		
		Label bookLabel = new Label("BookCopy Id");
		bookLabel.setAlignment(Pos.BASELINE_RIGHT);
		Label memberLabel = new Label("Library Member Id");
		memberLabel.setAlignment(Pos.BASELINE_RIGHT);
		TextField bookTF = new TextField();
		TextField memberTF = new TextField();
		
		gp.add(bookLabel, 1, 0);
		gp.add(memberLabel, 0, 0);
		gp.add(bookTF, 1, 1);
		gp.add(memberTF, 0, 1);
		
		final Pane pane = new Pane();
		pane.minHeightProperty().bind(memberLabel.heightProperty());
		gp.add(pane, 0, 2);
		
		Button checkOutBtn = new Button("Check Out");
		Button checkInBtn = new Button("Check In");
		Text message = new Text();

		gp.add(checkOutBtn, 0, 3);
		gp.add(checkInBtn, 1, 3);
		gp.add(message, 0, 4);
		
		checkOutBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				try {
					librarian.checkOutBook(Integer.parseInt(memberTF.getText()), bookTF.getText());
				} catch (Exception e) {
					message.setFill(Color.RED);
					message.setText(e.getMessage());
				} finally {
					resetTableData();
				}
			}	
		});
		
		checkOutBtn.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent arg0) {
				try {
					librarian.checkInBook(Integer.parseInt(memberTF.getText()), bookTF.getText());
				} catch (Exception e) {
					message.setFill(Color.RED);
					message.setText(e.getMessage());
				} finally {
					resetTableData();
				}
			}
		});
		
		return gp;
	}
	
	public static class TableEntry {
		private final String pattern = "MM/dd/yyyy";
		private final SimpleDateFormat dateformat = new SimpleDateFormat(pattern);
		private final SimpleStringProperty libraryMemberId;
		private final SimpleStringProperty bookCopyNumber;
		private final SimpleStringProperty checkOutDate;
		private final SimpleStringProperty dueDate;
		private final SimpleStringProperty checkInDate;
		public TableEntry(int memId, String copy, Date out, Date due, Date in ) {
			libraryMemberId = new SimpleStringProperty(Integer.toString(memId));
			bookCopyNumber = new SimpleStringProperty(copy);
			checkOutDate = new SimpleStringProperty(dateformat.format(out));
			dueDate = new SimpleStringProperty(dateformat.format(due));
			if (in == null) checkInDate = new SimpleStringProperty("-");
			else checkInDate = new SimpleStringProperty(dateformat.format(in));
		}
		public String getLibraryMemberId() {
			return libraryMemberId.get();
		}
		public String getBookCopyNumber() {
			return bookCopyNumber.get();
		}
		public String getCheckOutDate() {
			return checkOutDate.get();
		}
		public String getDueDate() {
			return dueDate.get();
		}
		public String getCheckInDate() {	
			return checkInDate.get();
		}
	}
	
	private void resetTableData() {
		collectData();
		this.tv.setItems(this.data);
	}
	
	private void collectData() {
		try {
			this.members = conn.getAll();
			for (LibraryMember member: members) 
				for (CheckOutEntry entry: member.getCheckOutRecord()) 
					data.add(new TableEntry(member.getId(), 
											entry.getBookCopy().getCopyNumber(), 
											entry.getStartDate(),
											entry.getDueDate(), 
											entry.getCheckInDate()) );
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
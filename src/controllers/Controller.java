package controllers;

import entities.*;
import java.util.List;
public interface Controller<T> {

	public List<T> index();
	public <T> boolean create(T member);
	public boolean update(T member);
	public boolean delete(T member);
	
}

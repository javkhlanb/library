package entities;

public class BookCopy {
	private String copyNumber;
	private int book_id;
	private int id;
	
	public int getId() {
		return id;
	}
	
	public void setBookId(int id) {
		this.book_id = id;
	}
	
	public int getBookId() {
		return book_id;
	}
	
	public void setId(int id) {
		this.id = id;
	}

	public String getCopyNumber() {
		return copyNumber;
	}

	public void setCopyNumber(String copyNumber) {
		this.copyNumber = copyNumber;
	}
}

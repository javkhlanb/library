package entities;
import java.util.Date;

public class CheckOutEntry {
	private static int entrycounter = 0;
	
	private int entryId;
	private LibraryMember librarymember;
	private BookCopy bookcopy;
	private Date startDate;
	private Date dueDate;
	private Date checkInDate;
	
	public CheckOutEntry(LibraryMember mem, BookCopy copy) {
		this.entryId = entrycounter++;
	
		this.librarymember = mem;
		this.bookcopy = copy;
		this.startDate = new Date();
		this.checkInDate = null;
		
		this.dueDate = new Date(); // + duration	
		//int duration = copy.getBook().getDuration();

	}
	public BookCopy getBookCopy() {
		return bookcopy;
	}
	public void checkIn() {
		this.checkInDate = new Date();
	}
	public boolean isCheckedIn() {
		return checkInDate == null ? false:true;
	}
	
	public Date getStartDate() {
		return startDate;
	}
	public Date getDueDate() {
		return dueDate;
	}
	public Date getCheckInDate() {
		return checkInDate;
	}
	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	public void setDueDate(Date dueDate) {
		this.dueDate = dueDate;
	}
	public void setCheckInDate(Date checkInDate) {
		this.checkInDate = checkInDate;
	}

}


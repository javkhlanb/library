package ui;

import java.io.IOException;
import java.sql.SQLException;

import entities.User;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.text.Text;
import javafx.stage.Stage;


public class LoginController {
	
	private String UniqueId;
	private String password;
	

	 @FXML 
	 private Text actiontarget;
	 
	 @FXML
	 private TextField user_id;
	 
	 
	 @FXML
	 private PasswordField passwordField;
	 
	 
	 @FXML
	 private ProgressIndicator progress;
	    
	 @FXML protected void handleSubmitButtonAction(ActionEvent event) {
	    	
		 
		 progress.setVisible(true);
		 UniqueId= user_id.getText();
		 password=passwordField.getText();
		 
		 
		 if(UniqueId.isEmpty() || password.isEmpty()) {
			 actiontarget.setText("Please  fill all credentials..");
			 progress.setVisible(false);
			 return;
		 }
			
			 
		 
	      //this  would be an asynchronous call over the net or to the db.
		 
		 User user;
		try {
			user = getUserById(Integer.valueOf(UniqueId));
			
			if(user==null) {
				actiontarget.setText("User does not exists!");
				 progress.setVisible(false);
				return;
			}
			
			if(verifyUser(user)) {
				loadConsole(event);
				 actiontarget.setText("Login in successful as a.."+user.getRole());
			}else {
				
				 actiontarget.setText("Incorrect password!!");
				
			}
			
			 progress.setVisible(false);
		} catch (NumberFormatException | ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			actiontarget.setText("In  error occured!!");
			 progress.setVisible(false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 
	    }
	    
	    
		public User getUserById(int id) throws ClassNotFoundException, SQLException {
			return DBconnection.getUser(id);
		}
		
		private boolean verifyUser(User user) {
			
			//user not verified.   //show error else get role
			return user.getPassword().equals(password);
		}
		
		
		
		private void loadConsole(ActionEvent event) throws IOException {
		  Parent root = FXMLLoader.load(getClass().getResource("console.fxml"));
		 Scene scene=new Scene(root);
		  
		  Stage stage=(Stage) ((Node) event.getSource()).getScene().getWindow();
		 stage.setTitle("Console");
		  
		  stage.setScene(scene);
		  stage.show();
		}

		
}

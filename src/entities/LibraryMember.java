package entities;

import java.util.ArrayList;

public class LibraryMember {
	
	private int memberId;
	private String firstName;
	private String lastName;
	private String phone;
	private Address address;
	private ArrayList<CheckOutEntry> checkOutRecord;
	
	public LibraryMember() {
		this.checkOutRecord = new ArrayList<CheckOutEntry>();
	}
	
	public int getId() {
		return memberId;
	}
	
	public void setId(int memberId) {
		this.memberId = memberId;
	}
	
	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getPhone() {
		return phone;
	}
	
	public void setPhone(String phone) {
		this.phone = phone;
	}
	
	public Address getAddress() {
		return address;
	}
	
	public void setAddress(Address address) {
		this.address = address;
	}
	
	public void checkOut(BookCopy bookCopy) {
		CheckOutEntry entry = new CheckOutEntry(this, bookCopy);
		checkOutRecord.add(entry);
	}
	
	public CheckOutEntry getCheckOutEntry(BookCopy bookCopy) {
		for (CheckOutEntry entry: checkOutRecord)
			if (entry.getBookCopy().getCopyNumber() == bookCopy.getCopyNumber())
				return entry;
		return null;
	}
	
	public void checkIn(String bookCopyNumber) {
		for (int i = 0; i < checkOutRecord.size(); i++) {
			CheckOutEntry entry = checkOutRecord.get(i);
			if(entry.getBookCopy().getCopyNumber() == bookCopyNumber &&
				entry.isCheckedIn() == false) 
				entry.checkIn();
		}
	}

	public ArrayList<CheckOutEntry> getCheckOutRecord() {
		return checkOutRecord;
	}

}

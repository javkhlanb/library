package models;
import java.util.HashMap;
import java.util.Map;

public class DataAccess<K, V> implements IDataAccess<K,V> {
	private Map<K, V> map = new HashMap<K, V>();
	@Override
	public V create(K key, V value) {
		return map.put(key, value);
	}

	@Override
	public V read(K key) {
		return map.get(key);
	}

	@Override
	public V update(K key, V value) {
		return map.put(key, value);
	}

	@Override
	public V delete(K key) {
		return map.remove(key);
	}
}

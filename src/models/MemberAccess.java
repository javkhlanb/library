package models;

import java.util.ArrayList;
import java.util.List;
import java.sql.*;

import entities.Address;
import entities.LibraryMember;

public class MemberAccess {
	
	private static Connection myCon;
	
	public MemberAccess() {
		try {
			myCon = DriverManager.getConnection("jdbc:mysql://localhost:3306/library", "root", "abc123");
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
	}
	
	public LibraryMember add(LibraryMember member) {
		try {
			Address address = new AddressAccess().add(member.getAddress());
			
			PreparedStatement ps = myCon.prepareStatement("INSERT INTO library_members (first_name,last_name,phone,address_id) VALUES(?,?,?,?)",Statement.RETURN_GENERATED_KEYS);
			ps.setString(1, member.getFirstName());
			ps.setString(2, member.getLastName());
			ps.setString(3, member.getPhone());
			ps.setInt(4, address.getId());
			ps.executeUpdate();
			
			ResultSet rs = ps.getGeneratedKeys();
			int id = 0;
			if(rs.next())
				id = rs.getInt(1);
			member.setId(id);
			return member;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
		
		return null;
	}
	
	public LibraryMember get(int id) {
		try {
			Statement statement = myCon.createStatement();
			ResultSet RS = statement.executeQuery("SELECT * FROM library_members WHERE id="+id);
			while(RS.next()) {
				LibraryMember member = new LibraryMember();
				member.setId(RS.getInt("id"));
				member.setFirstName(RS.getString("first_name"));
				member.setLastName(RS.getString("last_name"));
				member.setPhone(RS.getString("phone"));
				
				int addr = RS.getInt("address_id");
				if(addr > 0) {
					Address address = new AddressAccess().get(addr);
					member.setAddress(address);
				}
				return member;
			}
			
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
				
		return null;
	}
	
	public List<LibraryMember> getAll() {
		List<LibraryMember> members = new ArrayList<LibraryMember>();
		try {
			Statement statement = myCon.createStatement();
			ResultSet RS = statement.executeQuery("SELECT * FROM library_members");
			while(RS.next()) {
				LibraryMember member = new LibraryMember();
				member.setId(RS.getInt(RS.getInt("id")));
				member.setFirstName(RS.getString("first_name"));
				member.setLastName(RS.getString("last_name"));
				member.setPhone(RS.getString("phone"));
				
				int addr = RS.getInt("address_id");
				if(addr > 0) {
					Address address = new AddressAccess().get(addr);
					member.setAddress(address);
				}
				 members.add(member);
			}
			return members;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}		
		
		return null;
	}
	
	public LibraryMember update(LibraryMember member) {

		try {
			Address address = new AddressAccess().update(member.getAddress());

			PreparedStatement ps = myCon.prepareStatement("UPDATE library_members SET first_name=?,last_name=?,phone=?,address_id=? WHERE id=?");
			ps.setString(1, member.getFirstName());
			ps.setString(2, member.getLastName());
			ps.setString(3, member.getPhone());
			ps.setInt(4, member.getAddress().getId());
			ps.setInt(5, member.getId());
			ps.executeUpdate();
			
			member.setAddress(address);
			return member;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
		
		return null;
	}
	
	public LibraryMember delete(LibraryMember member) {
		
		try {
			PreparedStatement ps = myCon.prepareStatement("DELETE FROM library_members  WHERE id=?");
			ps.setInt(1, member.getId());
			ps.executeUpdate();
			
			Address address = new AddressAccess().delete(member.getAddress());
			ps = myCon.prepareStatement("DELETE FROM addresses WHERE id=?");
			ps.setInt(1, member.getAddress().getId());
			ps.executeUpdate();		
			
			member.setAddress(address);
			return member;
			
		} catch (SQLException e) {
			System.out.println("Error in connection!");
			e.printStackTrace();
		}
		
		return null;
	}

}

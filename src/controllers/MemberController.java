package controllers;

import java.util.ArrayList;
import java.util.List;

import entities.Address;
import entities.LibraryMember;
import models.*;

public class MemberController {
	
	private static MemberController instance = new MemberController();
	private static MemberAccess data = new MemberAccess();
	
	private MemberController() {}
	
	public MemberController getInstance() {
		return instance;
	}
	
	public List<LibraryMember> index(){
		List<LibraryMember> members = data.getAll();
		return members;
	}
	
	public LibraryMember fetch(int id) {
		LibraryMember member = data.get(id);
		return member;
	}
	
	public LibraryMember create(String fname, String lname, String phone, String street, String city, String state, String zip) {
		Address addr = new Address();
		addr.setStreet(street);
		addr.setCity(city);
		addr.setState(state);
		addr.setZip(zip);
		LibraryMember member = new LibraryMember();
		member.setFirstName(fname);
		member.setLastName(lname);
		member.setPhone(phone);
		member.setAddress(addr);
		member = data.add(member);
		return member;
	}
	public LibraryMember update(LibraryMember member, String fname, String lname, String phone, String street, String city, String state, String zip) {
		
		Address addr = member.getAddress();
		addr.setStreet(street);
		addr.setCity(city);
		addr.setState(state);
		addr.setZip(zip);
		
		member.setFirstName(fname);
		member.setLastName(lname);
		member.setPhone(phone);
		member.setAddress(addr);
		member = data.add(member);
		return member;
	}
	
	public LibraryMember delete(LibraryMember member) {
		member = data.delete(member);
		return member;
	}

}

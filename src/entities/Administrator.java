package entities;

import java.util.List;

import entities.*;
import controllers.*;

public class Administrator extends Role {
	
	public boolean addMember(LibraryMember member) {
		return false;
	}
	
	public boolean editMember(LibraryMember member) {
		
		
		return false;
	}
	
	public void addBook(Book b)
	{
		BookController.getInstance().create(b.getTitle(),b.getNumberOfDays() , b.getIsbn(), 
				b.getAuthors(), b.isAvailability(), b.getCopies());
	}
	
	public void deleteBook(Book b)
	{
		BookController.getInstance().delete(b);
	}
	
	public void editBook(Book book, String title, int numberOfDays, String isbn, List<Author> authors, boolean availability,
			List<BookCopy> copies)
	{
		BookController.getInstance().update(book, title, numberOfDays, isbn, authors, availability, copies);
	}

}

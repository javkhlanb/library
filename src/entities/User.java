package entities;


public class User {
	
	private int Id;
	private int user_id;
	private String password;
	private  String first_name;
	private String last_name;
	private Role role;
	
	
	
	public User() {
		
	}
	
	public User(int user_id, String password, String first_name, String last_name, Role role) {
		super();
		this.user_id = user_id;
		this.password = password;
		this.first_name = first_name;
		this.last_name = last_name;
		this.role = role;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public Role getRole() {
		return role;
	}
	public void setRole(Role role) {
		this.role = role;
	}
	@Override
	public String toString() {
		return "User [Id=" + Id + ", user_id=" + user_id + ", password=" + password + ", first_name=" + first_name
				+ ", last_name=" + last_name + ", role=" + role + "]";
	}
	
}
